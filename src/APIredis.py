import redis
import json

r = redis.Redis(
    host='localhost',
    port=6379,
    db=0)

def post_datan(key, value):
    r.set(key, value)
    return 1

def get_datan(key):
    retrieve_values = r.get(f'person{key}')
    retrieve_load = json.loads(retrieve_values)
    return retrieve_load

def get_all_datan():
    list_of_keys = r.keys('person*')
    dict_of_keys = {}
    for key in list_of_keys:
        dict_of_keys[key.decode()] = r.get(key).decode()
    return dict_of_keys

def put_datan(key, value):
    r.set(key, value)

def delete_datan(key):
    r.delete(f'person{key}')
