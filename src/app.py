from flask import Flask, jsonify, abort, make_response, request
import APIredis
import json


app = Flask(__name__)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

#POST OR GET ALL
@app.route('/data/api/v1.0/', methods=['POST', 'GET'])
def get_set_data():
    if request.method == 'POST':
        if not request.json or not request.json['key']:
            abort(400)
        if APIredis.r.exists(request.json['key']):
            abort(409)
        key = request.json['key']
        payload = json.dumps(request.json)
        APIredis.post_datan(key, payload)

        return f' {payload}', 201
    if request.method == 'GET':
        try:
            result = APIredis.get_all_datan()
            return jsonify(result)
        except:
            abort(404)

#PUT
@app.route('/data/api/v1.0/<int:key>', methods=['PUT'])
def update_data(key):
    if not request.json or not request.json['key']:
            abort(400)
    if not APIredis.r.exists(request.json['key']):
            abort(409)
    key = request.json['key']
    payload = json.dumps(request.json)
    APIredis.post_datan(key, payload)

    return f' {payload}', 201

#GET SPECIFIC OR DELETE SPECIFIC
@app.route('/data/api/v1.0/<int:key>', methods=['GET', 'DELETE'])
def get_or_delete_data(key):
    if request.method == 'GET':
        if not APIredis.r.exists(f'person{key}'):
            abort(409)
        try:
            result = APIredis.get_datan(key)
            return jsonify(result)
        except:
            abort(404)
    if request.method == 'DELETE':
        if not request.json or not request.json['key']:
            abort(400)
        if not APIredis.r.exists(f'person{key}'):
            abort(409)
        try:
            APIredis.delete_datan(key)
            return make_response(jsonify({f'{request.json["key"]}': 'Deleted'}), 201)
        except:
            abort(404)

@app.route('/docs', methods=['GET'])
def index_docs():
    return make_response(jsonify({'Docs': 'found'}), 200)

if __name__ == '__main__':
    app.run(debug=True)