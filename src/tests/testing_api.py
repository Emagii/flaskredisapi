import requests
import redis
import json

r = redis.Redis(
    host='localhost',
    port=6379,
    db=0)

headers = {'content-type' : 'application/json'}
data = {
    'key': 'person1',
    'name': 'Dennis'
}
'''
resp = requests.post('http://localhost:5000/data/api/v1.0/',
                    headers=headers,
                    json=data)
'''

resp = requests.get('http://localhost:5000/data/api/v1.0/')


print(resp.text)
'''
list_of_keys = r.keys('person*')

for key in list_of_keys:
    print(r.get(key))

dict_of_keys = {}
for key in list_of_keys:
    dict_of_keys[key.decode()] = r.get(key).decode()

print(dict_of_keys)
'''