import unittest
import redis
import json
from APIredis import get_datan, get_all_datan, post_datan


class TestStringMethods(unittest.TestCase):

    def setUp(self):
        #Connecting
        r = redis.Redis(
        host='localhost',
        port=6379,
        db=0)

        #Making 2 entries
        data = {
        'key': 'person2',
        'name': 'Jennie'
        }
        payload = json.dumps(data)
        r.set('person2', payload)

        data = {
        'key': 'person1',
        'name': 'Dennis'
        }
        payload = json.dumps(data)
        r.set('person1', payload)

    def tearDown(self):
        r = redis.Redis(
        host='localhost',
        port=6379,
        db=0)
        
        r.flushdb()
    
    def test_post_datan(self):
        data = {
        'key': 'person1',
        'name': 'Dennis'
        }
        payload = json.dumps(data)
        self.assertEqual(post_datan('person1', payload), 1)

    def test_exists_in_redis(self):
        payload = get_datan('1')
        self.assertEqual(payload['name'], 'Dennis')

    def test_get_all_datan(self):
        payload = get_all_datan()
        self.assertEqual(payload, dict({'person1': '{"key": "person1", "name": "Dennis"}',
                                        'person2': '{"key": "person2", "name": "Jennie"}'}))


if __name__ == '__main__':
    unittest.main()